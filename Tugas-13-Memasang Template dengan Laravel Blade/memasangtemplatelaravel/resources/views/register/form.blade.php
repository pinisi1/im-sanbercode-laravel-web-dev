<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
</head>
<body>
    <form action="/kirim" method="post">
        @csrf
        <p><h1>Buat Account Baru</h1></p>
        <p><h3>Sign Up Form</h3></p>
        <p>First Name :</p>
        <input type="text" name="first">
        <p>Last Name :</p>
        <input type="text" name="last">
        <p>Gender :</p>
        <input type="radio" name="male">Male <br>
        <input type="radio" name="female">Female <br>
        <input type="radio" name="other">Other
        <p>Nationality :</p>
        <select name="national" >
            <option value="1">Indonesia</option>
            <option value="2">Singapure</option>
            <option value="3">Malaysia</option>
            <option value="4">Australia</option>
        </select>
        <p>Language Spoken :</p>
        <input type="radio" name="indo">Bahasa Indonesia <br>
        <input type="radio" name="english">English <br>
        <input type="radio" name="other" >Other <br>
        <p>Bio :</p>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="kirim">
    </form>
</body>
</html>