<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function formku(){
        return view("/register.form");
    }

    public function kirim(Request $request){
        $namafirst = $request->input('first');
        $namalast = $request->input('last');
        return view('/register.home', ["namafirst" => $namafirst, "namalast" => $namalast]);
    }
}
