<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Tugas 12
// Route::get('/', [HomeController::class, 'home']);
// Route::get('/form', [AuthController::class, 'formku']);
// Route::post('/kirim', [Authcontroller::class, 'kirim']);

// Testing
// Route::get('/master', function() {
//     return view('master');
// });

// Tugas 13
Route::get('/', [DashboardController::class, 'home']); //Index
Route::get('/table', [DashboardController::class, 'home']); //table
// data-table
Route::get('/data-table', function() {
    return view('table.data-table');
});

// Tugas 15 - CRUD Cast
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);



