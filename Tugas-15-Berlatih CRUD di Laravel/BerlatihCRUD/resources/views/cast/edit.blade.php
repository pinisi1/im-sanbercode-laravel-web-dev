@extends('master')
@section('judul')
    Halaman Edit Cast BerId {{$cast->id}}
@endsection
@section('isi')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        {{-- Coloum Nama --}}
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan Title">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        {{-- Coloum Umur --}}
        <div class="form-group">
            <label for="title">Umur</label>
            <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="title" placeholder="Masukkan Title">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        {{-- Coloum Bio --}}
        <div class="form-group">
            <label for="title">Bio</label>
            <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="title" placeholder="Masukkan Title">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection