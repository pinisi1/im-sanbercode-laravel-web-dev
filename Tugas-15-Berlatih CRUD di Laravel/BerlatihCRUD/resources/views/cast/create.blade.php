@extends('master')
@section('judul')
    Halaman Tambah Cast
@endsection
@section('isi')
    <form action="/cast" method="POST">
        @csrf
        {{-- Coloum Nama --}}
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        {{-- Coloum Umur --}}
        <div class="form-group">
            <label for="title">Umur</label>
            <input type="text" class="form-control" name="umur" id="title" placeholder="Masukkan Title">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        {{-- Coloum Bio --}}
        <div class="form-group">
            <label for="title">Bio</label>
            <input type="text" class="form-control" name="bio" id="title" placeholder="Masukkan Title">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection