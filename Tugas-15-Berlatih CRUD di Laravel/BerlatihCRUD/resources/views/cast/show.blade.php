@extends('master')
@section('judul')
    Halaman Detail Cast Id {{$cast->id}}
@endsection
@section('isi')
    <h4>Nama Cast : {{$cast->nama}}</h4>
    <h4>Umur Cast : {{$cast->umur}}</h4>
    <h4>Bio Cast : {{$cast->bio}}</h4>
@endsection